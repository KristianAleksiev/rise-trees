﻿using BinaryTree;

namespace PostOrder
{
    public class PostOrder
    {
        static void Main(string[] args)
        {
            
            var node24 = new TreeNode(24, null, null);
            var node11 = new TreeNode(11, null, node24);

            var node35 = new TreeNode(35, null, null);
            var node61 = new TreeNode(61, null, null);
            var node76 = new TreeNode(76, null, null);

            var node38 = new TreeNode(38, node35, null);
            var node69 = new TreeNode(69, node61, node76);
            var node52 = new TreeNode(52, node38, node69);

            //Root
            var rootNode25 = new TreeNode(25, node11, node52);

            int treeLength = LengthOfTree(rootNode25);

            Console.WriteLine($"The length of the tree is {treeLength}");
        }

        public static int LengthOfTree(TreeNode rootNode)
        {
            if (rootNode == null)
            {
                return 0;           
            }                      
            int leftSide = LengthOfTree(rootNode.Left);
            int rightSide = LengthOfTree(rootNode.Right);

            return 1 + Math.Max(leftSide, rightSide);
            


        }
    }
}

