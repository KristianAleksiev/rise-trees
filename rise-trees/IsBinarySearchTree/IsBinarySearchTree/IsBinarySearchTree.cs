﻿using BinaryTree;
using System.Xml.Linq;


namespace BinarySearchTreeCheck
{
    public class BinarySearchTreeCheck
    {
        static void Main(string[] args)
        {
            var node24 = new TreeNode(24, null, null);
            var node11 = new TreeNode(11, null, node24);
            //var node11 = new TreeNode(26, null, node24);

            var node35 = new TreeNode(35, null, null);
            var node61 = new TreeNode(61, null, null);
            var node76 = new TreeNode(76, null, null);

            var node38 = new TreeNode(38, node35, null);
            var node69 = new TreeNode(69, node61, node76);

            var node52 = new TreeNode(52, node38, node69);


            var rootNode25 = new TreeNode(25, node11, node52);

            int minSysValue = int.MinValue;
            int maxSysValue = int.MaxValue;



            bool result = IsBinarySearchTree(rootNode25, minSysValue, maxSysValue);

            if (result )
            {
                Console.WriteLine("The binary tree is a binary search tree");
            }
            else
            {
                Console.WriteLine("The binary tree is not a binary search tree");
            }
        }

        public static bool IsBinarySearchTree(TreeNode rootNode, int minSysValue, int maxSysValue)
        {

            if (rootNode == null)
            {
                return true;
            }

            if (rootNode.Value < minSysValue || rootNode.Value > maxSysValue)
            {
                return false;
            }
                             
            return IsBinarySearchTree(rootNode.Left, minSysValue, rootNode.Value - 1) &&
                   IsBinarySearchTree(rootNode.Right, rootNode.Value + 1, maxSysValue);

        }
    }
}
