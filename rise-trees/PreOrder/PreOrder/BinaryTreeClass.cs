﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryTree
{
    public class TreeNode
    {
        public TreeNode(int value, TreeNode left, TreeNode right)
        {
            Value = value;
            Left = left;
            Right = right;
        }

        public int Value { get; set; }

        public TreeNode Left { get; set; }

        public TreeNode Right { get; set; }


    }
}


