﻿using BinaryTree;

namespace PostOrder
{
    public class PostOrder
    {
        static void Main(string[] args)
        {

            // Left 25 - 11 - 24(r)
            // Right 25 - 52 - 38(l)35(l) - 69(r)61(l)76(r)

            //Left
            var node24 = new TreeNode(24, null, null);
            var node11 = new TreeNode(11, null, node24);

            //Right
            // L3
            var node35 = new TreeNode(35, null, null);
            var node61 = new TreeNode(61, null, null);
            var node76 = new TreeNode(76, null, null);
            //L2 
            var node38 = new TreeNode(38, node35, null);

            var node69 = new TreeNode(69, node61, node76);

            //L1
            var node52 = new TreeNode(52, node38, node69);

            //Root
            var rootNode25 = new TreeNode(25, node11, node52);

            PostOrderAlgo(rootNode25);
        }

        public static void PostOrderAlgo(TreeNode rootNode)
        {
            if (rootNode != null)
            {
                PostOrderAlgo(rootNode.Left);
                PostOrderAlgo(rootNode.Right);
                Console.Write(rootNode.Value + " ");

            }
        }
    }
}

